This is just a simple test.

1. Check out the test project
git clone https://bitbucket.org/HaoDeng/test.git

2. Functional Requirements

* Click "submit" button shall check that all questions shall be answered. 
Unanswered questions shall be highlighted.
* If all questions are selected, click "submit" button redirects to feedback page.

3. Styling test.html and feedback.html pages, make it good looking.


The judgement is based on:

1. Functioning

2. Good looking

3. Code formatting